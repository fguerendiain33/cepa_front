import { Component, Input, OnInit } from '@angular/core';
import { Product } from 'src/app/entities/product/productState/product.state.model';

@Component({
  selector: 'app-product-row',
  templateUrl: './product-row.component.html',
  styleUrls: ['./product-row.component.scss']
})
export class ProductRowComponent implements OnInit {

  @Input() product : Product;
  
  selectedProduct: Product;
  quantity: number = 0;

  constructor(
  ) { }

  ngOnInit(): void {
  }


  onRowSelect(event: any) {
    
  }


}
