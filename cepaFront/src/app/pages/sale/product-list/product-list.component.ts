import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { ProductService } from 'src/app/entities/product/product.service';
import { Product } from 'src/app/entities/product/productState/product.state.model';
import * as fromProductSelect from '../../../entities/product/productState/product.state.selector'

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {

  productList$: Observable<Array<Product>> = this.store.select(fromProductSelect.getProducts)

  constructor(
    private store: Store,
    private productService: ProductService
  ) { }

  ngOnInit(): void {
    this.productService.getProducts();
  }

}
