import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ProductService } from 'src/app/entities/product/product.service';
import { Product } from 'src/app/entities/product/productState/product.state.model';
import * as fromProductSelect from '../../../entities/product/productState/product.state.selector';
import { DropdownDataItem } from '../../../shared/shared.model';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {

  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1);

  products: Array<Product>;
  headings: Array<DropdownDataItem>;

  loading: boolean = true;

  activityValues: number[] = [0, 100];

  constructor(
    private productService: ProductService,
    private store: Store
  ) { }

  ngOnInit() {
    this.getProducts();
    this.getProductsList();
    this.getHeadingList();
    this.loading = false;
  }


  ngOnDestroy(): void {
    this.destroyed$.next(true);
    this.destroyed$.complete();
  }

  getProducts(): void{
    this.productService.getProducts();
  }

  getProductsList(){
    this.store
    .pipe(takeUntil(this.destroyed$),select(fromProductSelect.getProducts))
    .subscribe( products => {
        this.products = products;
        }
      )
  }  

  getHeadingList(){
    this.store
    .pipe(takeUntil(this.destroyed$),select(fromProductSelect.getHeadings))
    .subscribe( headings => {
      headings.forEach(head => {
        this.headings.push({
          name: head,
          code: head
        });
      })
    })
  }


  clear(table: any) {
      table.clear();
  }
}
